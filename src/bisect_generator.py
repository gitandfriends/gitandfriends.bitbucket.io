from sys import argv
from os import path as osp
from random import choice
from string import ascii_lowercase as letters
from git import Repo

if len(argv) < 2 or 'simple-prompt' in argv[1]:
    argv = 'executable /tmp repo-name 1000 bug 492 problem 381 mistake 911 error 694'.split()

args = iter(argv)
next(args) # skip executable
repo_parent_directory = next(args)
repo_name             = next(args)
total_commits     = int(next(args))
file_name         = 'the-file'
bugs = {}

for bug in args:
    first_commit_containing_bug = int(next(args))
    bugs[bug] = first_commit_containing_bug


repo_dir     = osp.join(repo_parent_directory, repo_name)
fq_file_name = osp.join(repo_dir, file_name)
repo = Repo.init(repo_dir)

for commit_number in range(1, total_commits+1):
    bugs_in_this_commit = [ bug for (bug, first_commit) in bugs.items()
                            if commit_number >= first_commit ]
    with open(fq_file_name, 'w') as the_file:
        print('{:5d} {}'.format(commit_number, ' '.join(bugs_in_this_commit)), file=the_file)

    noise1 = ''.join(choice(letters) for _ in range(18))
    hint = chr(ord('a') + len(bugs_in_this_commit))
    commit_message = '{}{}{:5d}'.format(noise1, hint, commit_number)

    repo.index.add([fq_file_name])
    repo.index.commit(commit_message)

function commit {
    FILENAME=$1
    MESSAGE=$2
    touch $FILENAME
    git add $FILENAME
    git commit -m "${MESSAGE}"
}

function make_history {
    git init ${REPO_NAME}
    cd ${REPO_NAME}
    commit m1 "ancient history"
    # Branch off master to work on amazing feature
    git checkout -b amazing
    commit a1 "amazing 1"
    commit a2 "amazing 2"
    commit a3 "amazing 3"
    # Meanwhile, work is also being done on brilliant feature
    git checkout -b brilliant master
    commit b1 "brilliant 1"
    commit b2 "brilliant 2"
    # Brilliant is complete and gets merged into master
    FINISH_FEATURE brilliant
    # Back on amazing, you don't want to diverge too much from master
    # or maybe you need to use brilliant in your ongoing work on
    # amazing, so you 'pull' master'
    git checkout amazing
    PULL_MASTER_INTO_FEATURE
    # ... before proceeding with further work on amazing
    commit a4 "amazing 4"
    commit a5 "amazing 5"
    # Meanwhile, other work is being done elsewhere
    git checkout -b cool master
    commit c1 "cool 1"
    commit c2 "cool 2"
    commit c3 "cool 3"
    # Lots of parallel development takes place
    git checkout -b desirable master
    commit d1 "desirable 1"
    commit d2 "desirable 2"
    # Desirable was easy, so it gets merged quickly
    FINISH_FEATURE desirable
    # But work continues on the other branches
    git checkout cool
    commit c4 "cool 4"
    commit c5 "cool 5"
    # and we don't want to diverge too much from master, so we pull
    PULL_MASTER_INTO_FEATURE
    # ... before continuing work
    commit c6 "cool 6"
    commit c7 "cool 7"
    # Something similar happens on amazing: work
    git checkout amazing
    commit a6 "amazing 6"
    # ... pull in master
    PULL_MASTER_INTO_FEATURE
    # ... work some more
    commit a7 "amazing 7"
    # Cool is now finished, and gets merged into master
    FINISH_FEATURE cool
    # but amazing continues
    git checkout amazing
    PULL_MASTER_INTO_FEATURE
    commit a8 "amazing 8"
    # while work starts on effective
    git checkout -b effective master
    commit e1 "effective 1"
    commit e2 "effective 2"
    # Effective turns out to be simple, so it's merged immediately
    FINISH_FEATURE effective
    # but some more work needs to be done on amazing
    git checkout amazing
    commit a9 "amazing 9"
    # before it is finished and merged
    FINISH_FEATURE amazing

    cd ..
}

########################################
REPO_NAME=merge

function FINISH_FEATURE {
    git checkout master
    git merge --no-edit $1
    git branch -d $1
}
function PULL_MASTER_INTO_FEATURE { git merge --no-edit master; }
make_history

########################################
REPO_NAME=rebase

function FINISH_FEATURE {
    git rebase master $1
    git checkout master
    git merge --no-edit $1
    git branch -d $1
}
function PULL_MASTER_INTO_FEATURE { git rebase master; }
make_history

########################################
REPO_NAME=rebase-no-ff

function FINISH_FEATURE {
    git rebase master $1
    git checkout master
    git merge --no-edit --no-ff $1
    git branch -d $1
}
function PULL_MASTER_INTO_FEATURE { git rebase master; }
make_history
